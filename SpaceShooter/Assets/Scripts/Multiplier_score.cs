﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiplier_score : MonoBehaviour {
	public GameObject explosin;
	//public GameObject playerexplosin;
	//public int ScoreValue;
	private GameController gameController;
	void Start(){
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "boundary") {
			return;
		}
		Instantiate(explosin, transform.position, transform.rotation);
		if (other.tag == "Player") {
			//Instantiate (playerexplosin, other.transform.position, other.transform.rotation);
			gameController.Multiplier ();
		}
		//Destroy (other.gameObject);
		gameController.Multiplier();
		Destroy (gameObject);
		//gameController.addScore (ScoreValue);
	}
}
