﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float SpawnWait;
	public float StartWait;
	public float WaveLength;
	public GUIText ScoreText;
	public GUIText RestartText;
	public GUIText EndGameText;
	private bool gameover;
	private bool restart;
	private int Score;
	private bool multi;
	private bool protect;
	public GameObject Can;
	public GameObject Power;
	private int Rounds;
	private GameController gameController;
	public int wave;
	private float extra;
	void Start () {
		Score = 0;
		multi = false;
		protect = false;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		gameover = false;
		restart = false;
		RestartText.text = "";
		EndGameText.text = "";
		Rounds = 5;
		wave = 1;

	}
	void Update(){
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}
	IEnumerator SpawnWaves(){
		yield return new WaitForSeconds (StartWait);
		while (true) {
			for (int i = 0; i < hazardCount*wave; i++) {
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				extra = Random.Range (0, 50); 
				if (extra == 0 || extra==25) {
					if (i == 0) {
						Instantiate (Power, spawnPosition, spawnRotation);
					} else {
						Instantiate (Can, spawnPosition, spawnRotation);
					}
				} else {
					Instantiate (hazard, spawnPosition, spawnRotation);
				}
				yield return new WaitForSeconds (SpawnWait);
			}
			yield return new WaitForSeconds (WaveLength);
			wave += 1;
			if (gameover) {
				RestartText.text = "Press 'R' to Restart";
				restart = true;
				break;
			}
		}
	}
	public void addScore(int newScoreValue){
		if (multi) {
			Score += newScoreValue * 5;
			UpdateScore ();
		} else {
			Score += newScoreValue;
			UpdateScore ();
		}
	}
	public bool MultiOn(){
		return multi;
	}
	public void Multiplier(){
		multi = true;
		Invoke ("Multiplier_off", 10);
	}
	public void Multiplier_off(){
		multi = false;

	}

	// Update is called once per frame
	void UpdateScore () {
		ScoreText.text = "Score " + Score;
	}
	public void GameOver(){
		EndGameText.text = "Game Over";
		gameover = true;
	}
	public bool Protection(){
		return protect;
	}
	public void Protection_on(){
		protect = true;
		Invoke ("Protection_off", 5);
	}
	public void Protection_off(){
		protect = false;
	}
	public int Current_rounds(){
		return Rounds;
	}
	public void Round_add(){
		if (Rounds < 5) {
			Rounds += 1;
			print ("no");
		}
	}
	public void Round_off(){
		if (Rounds > 0) {
			Rounds -= 1;
			Invoke ("Round_add", 15);
		}
	}

}
