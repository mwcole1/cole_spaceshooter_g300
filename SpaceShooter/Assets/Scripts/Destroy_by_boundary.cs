﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_by_boundary : MonoBehaviour {

	void OnTriggerExit(Collider other)
	{
		// Destroy everything that leaves the trigger
		Destroy(other.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
